# Sweaty Promo

_Bibliotèque "client" du jeu Sweaty Promo_

## 1) Utilisation

__Installation (via npm)__

Exemple d'interface utilisateur utilisant le client du jeu
`npm install gitlab:simplon-grenoble-promo-1-formateurs/sweaty-promo-client`

Le fichier de distribution est dans le dossier _dist_

`<script src="./node_modules/sweaty-promo-client/dist/sweaty-promo-client.js" charset="utf-8"></script>`

__Exemple__

```
// instance de la class du jeu
var game = new SweatyPromoClient.offline()

/* ÉVÉNEMENTS */

// reception d'une nouvelle matrice des gouttes
game.on('matrix', (matrix) => {

})

// le joueur perd la partie
game.on('sweaty', () => {

})

/* METHODES */

// se déplacer vers la gauche
game.left()

// se déplacer vers la droite
game.right()

// entrer dans le jeu
game.start()

// mettre en pause le jeu
game.pause()

// relancer le jeu (après une pause)
game.play()

// récupérer la matrice courante
game.getMatrix()

// récupérer la position du joueur courante
game.getPosition()
```

À quoi ressemble la matrice ?

`console.log(game.getMatrix())`

```
[
	[0, 1, 0, 0, 0],
	[1, 0, 0, 0, 0],
	[0, 1, 0, 0, 0],
	[0, 0, 0, 0, 0],
	[0, 0, 0, 0, 0],
	[0, 0, 0, 0, 0],
	[0, 0, 0, 0, 0],
	[0, 0, 0, 0, 0]
]
```

C'est un tableau de 8 tableaux et chaque sous-tableau contient 5 chiffres.

- 1 correspond à une goutte

- 0 correspond à "vide"

La matrice est actualisée toutes les secondes (car les gouttes tombent)

## 2) Utilisation de la version en ligne

> ATTENTION : pour que la version en ligne fonctionne, il faut lancer la page HTML via un serveur en utilisant le port 5500.
> L'adresse dans le navigateur doit être : http://127.0.0.1:5500
>
> Le plus simple est d'utiliser l'extention de VS Code : LiveServer
> https://marketplace.visualstudio.com/items?itemName=ritwickdey.LiveServer
>
> Cette contrainte vient de l'authentification oAuth qui impose de prévoir une URL de callback.

La création de l'instance ce fait avec la méthode _online_

Il y a des événements et des méthodes propre à la version en ligne.

Certaines méthodes de la version hors ligne ne sont plus disponible (notemment la fonction pause).


__Exemple version en ligne__

_Présentation des nouvelles fonctions uniquement_

```
// instance de la class du jeu (online à la place de offline)
var game = new SweatyPromoClient.online()

/* ÉVÉNEMENTS */

// reception de la position courante
game.on('position', (position) => {

})

// reception d'un message
game.on('message', (messageData) => {
  // messageData est un objet avec 2 propriétés : "author" et "message"
})

/* METHODES */

// s'authentifier (oAuth Discord)
game.login()

// est authentifié ou non
game.isAuth()

// envoyer un message à tous
game.sendMessage(message)
```

## 3) Développement


__Installer les dépendances__

`npm install`

__Build (webpack)__

Pour lancer la création continue du fichier de distribution _/dist/sweaty-promo-client.js_, utiliser la commande :

`npm run watch`
