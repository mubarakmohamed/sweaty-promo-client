const webpack = require("webpack");
const path = require("path");

let config = {
  mode: "production",
  entry: "./src/js/script.js",
  output: {
    path: path.resolve(__dirname, "./dist"),
    filename: "./script.js",

  },
  module: {
    rules: [{
      test: /\.js$/,
      exclude: /node_modules/,
      loader: "babel-loader"
    }]
  },
  node: {
    fs: "empty"
  }
}

module.exports = config;
